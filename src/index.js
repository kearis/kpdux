import compose from "./utils/compose";
import mixin from "./utils/mixin";
import createStore from "./utils/createStore";
import createModule from "./utils/createModule";


export {
    compose,
    mixin,
    createStore,
    createModule
};

export default {
    "compose": compose,
    "mixin": mixin,
    "createStore": createStore,
    "createModule": createModule
};